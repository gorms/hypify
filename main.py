#!/usr/bin/python3

import urllib.request
import json

user = "cornchips"
api_req = "https://api.hypem.com/v2/users/"+user+"/favorites?count=9999999&key=swagger"

r = urllib.request.urlopen(api_req)
data = json.loads(r.read().decode(r.info().get_param('charset') or 'utf-8'))
print(json.dumps(data, indent=4))
